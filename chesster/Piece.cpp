#include <pch.h>
#include "Piece.h"
#include "Board.h"

std::vector<Move> Piece::getMoves(Board board, Pos selfPos) const {
	std::vector<Move> moves = getSeen(board, selfPos);

	moves.erase(std::remove_if(moves.begin(), moves.end(), [&](const Move& move) {
		return board.isMoveCheck(move);
		}), moves.end());

	return moves;
}


bool Piece::hasAnyAvailMove(Board board, Pos selfPos) const
{
	std::vector<Move> moves = getSeen(board, selfPos);

	return std::any_of(moves.begin(), moves.end(), [&](const Move& move) {
		return !board.isMoveCheck(move);
		});
}

std::vector<Move> Piece::getSeen(Board board, Pos selfPos) const
{
	std::vector<Move> out;

	const auto addContinuousMoves = [=, &out](int maxIteration, Pos diffMove)
	{
		for (int8_t i = 1; i <= maxIteration; ++i) {
			Pos availPos = selfPos + (i * diffMove);
			if (!inBoardBounds(availPos))
				continue;

			const Piece piece = board.getPiece(availPos);
			
			if (!piece.isNull()) {
				if (piece.getColor() != getColor())
					out.emplace_back(selfPos, availPos, availPos);

				return;
			}

			out.emplace_back(selfPos, availPos);
		}
	};

	const auto min = [](int8_t a, int8_t b) { return a < b ? a : b; };


	switch (type) {
	case PieceType::PAWN:
	{
		int8_t nDir = getColor() == Color::WHITE ? -1 : 1;

		Pos oneAhead{ selfPos + olc::vt2d{0, nDir * 1} };

		const auto putMove = [&](Pos to, Pos eat = NULL_POS) {
			if (!inBoardBounds(to)) return;
			if (!inBoardBounds(eat) && eat != NULL_POS) return;

			if (oneAhead.y() != 0 && oneAhead.y() != BOARD_SIZE - 1) {
				out.emplace_back(selfPos, to, eat);
			}
			else {
				for (PieceType promoteType : { PieceType::QUEEN, PieceType::BISHOP, PieceType::KNIGHT, PieceType::ROOK }) {
					out.emplace_back(selfPos, to, eat, promoteType);
				}
			}
		};
		
		if (board.getPiece(oneAhead).isNull()) {
			putMove(oneAhead);

			bool didntMove = selfPos.y() == 1 || selfPos.y() == BOARD_SIZE - 2;
			if (Pos twoAhead{ selfPos + olc::vt2d{0, nDir * 2} };
				board.getPiece(twoAhead).isNull() && didntMove)
				putMove(twoAhead);
		}

		for (int horizontalDir : {-1, 1}) {
			Pos availPos{ selfPos + olc::vt2d{(int8_t)horizontalDir, nDir} };
			const Piece piece = board.getPiece(availPos);
			if (inBoardBounds(availPos)) {
				// eat
				if (!piece.isNull() && piece.getColor() != getColor()) {
					putMove(availPos, availPos);
				}
				// un passant
				else if (availPos == board.getUnPassantPos()) {
					putMove(availPos, availPos + Pos(0, -nDir));
				}

			}
		}
		break;
	}
	case PieceType::ROOK:
	{
		// N
		addContinuousMoves(selfPos.y(), Pos{0, -1});

		// S
		addContinuousMoves(BOARD_SIZE - selfPos.y() - 1, Pos{0, 1});

		// W
		addContinuousMoves(selfPos.x(), Pos{-1, 0});

		// E 
		addContinuousMoves(BOARD_SIZE - selfPos.x() - 1, Pos{1, 0});
		break;
	}
	case PieceType::KNIGHT:
	{
		for (auto diff : {
			Pos{1, 2},   Pos{2, 1},
			Pos{-1, 2},  Pos{2, -1},
			Pos{1, -2},  Pos{-2, 1},
			Pos{-1, -2}, Pos{-2, -1}
		}) {
			Pos availPos = selfPos + diff;
			if (!inBoardBounds(availPos)) continue;

			const Piece eatingPiece = board.getPiece(availPos);
			if (eatingPiece.getColor() != getColor() || eatingPiece.isNull())
				out.emplace_back(selfPos, availPos, eatingPiece.isNull()  ? NULL_POS : availPos);
		}
		break;
	}
	case PieceType::BISHOP:
	{
		// NW
		addContinuousMoves(min(selfPos.x(), selfPos.y()), Pos{-1, -1});

		// NE
		addContinuousMoves(min(BOARD_SIZE - selfPos.x() - 1, selfPos.y()), Pos{1, -1});

		// SW
		addContinuousMoves(min(selfPos.x(), BOARD_SIZE - selfPos.y() - 1), Pos{ -1, 1 });

		// SE
		addContinuousMoves(min(BOARD_SIZE - selfPos.x(), BOARD_SIZE - selfPos.y()) - 1, Pos{ 1, 1 });

		break;
	}
	case PieceType::QUEEN:
	{
		// N
		addContinuousMoves(selfPos.y(), Pos{0, -1});

		// S
		addContinuousMoves(BOARD_SIZE - selfPos.y() - 1, Pos{0, 1});

		// W
		addContinuousMoves(selfPos.x(), Pos{-1, 0});

		// E 
		addContinuousMoves(BOARD_SIZE - selfPos.x() - 1, Pos{1, 0});

		// NW
		addContinuousMoves(min(selfPos.x(), selfPos.y()), Pos{-1, -1});

		// NE
		addContinuousMoves(min(BOARD_SIZE - selfPos.x() - 1, selfPos.y()), Pos{1, -1});

		// SW
		addContinuousMoves(min(selfPos.x(), BOARD_SIZE - selfPos.y() - 1), Pos{ -1, 1 });

		// SE
		addContinuousMoves(min(BOARD_SIZE - selfPos.x(), BOARD_SIZE - selfPos.y()) - 1, Pos{ 1, 1 });
		break;
	}
	case PieceType::KING:
	{
		for (int8_t y = -1; y <= 1; y++)
			for (int8_t x = -1; x <= 1; x++) {
				Pos availPos = selfPos + Pos(x, y);
				if (!inBoardBounds(availPos) || availPos == selfPos)
					continue;

				const Piece eatingPiece = board.getPiece(availPos);
				if (eatingPiece.getColor() != getColor() || eatingPiece.isNull())
					out.emplace_back(selfPos, availPos, eatingPiece.isNull() ? NULL_POS : availPos);
			}

		// castle
		// left - queen
		if (board.canCastle(Board::CastleAbility::QUEEN, getColor())) {
			out.emplace_back(selfPos, selfPos + Pos(-2, 0));
		}

		// right - king
		if (board.canCastle(Board::CastleAbility::KING, getColor())) {
			out.emplace_back(selfPos, selfPos + Pos(2, 0));
		}
		break;
	}
	default:
		PRINT_ERROR("unable to getSeen of type: " + std::string(magic_enum::enum_name(type)));
	}

	return out;
}
