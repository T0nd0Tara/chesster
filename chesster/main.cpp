#include "pch.h"

#define OLC_PGE_APPLICATION

#include "common.h"

#include "./Move.h"
#include "./Piece.h"
#include "Board.h"

#include "Com/Com.h"

#include "./FileHandler.h"

class Example : public olc::PixelGameEngine
{
public:
	Example()
	{
		sAppName = "Chesster";
	}

private:
	const olc::Pixel BG_BLACK = olc::Pixel(119, 149, 86);
	const olc::Pixel BG_WHITE = olc::Pixel(235, 236, 208);

	const olc::Pixel COL_HOVER = olc::RED;
	const olc::Pixel COL_SELECTED = olc::Pixel(255,255,64,128);
	const olc::Pixel COL_AVAIL = olc::Pixel(10, 255, 10, 128);

	Board board;
	Com com;

	int cellSize;

	std::unique_ptr<olc::Decal> dPieces;
	olc::vf2d sourceSize;

	olc::vt2d vSelectedCell{ -1, -1 };
	olc::vt2d vHoverCell{ -1, -1 };
	std::vector<Move> vAvailMoves;

	bool bRecordMoves = true;
	FileHandler moveRecord;

	bool bMate = false;
	bool bPromoting = false;
	bool bGameStarted = true;

	olc::vi2d vPrompotingConsolePos;
	olc::vi2d vPrompotingConsoleSize;
	int8_t hoverPromoteIndex = -1;
	Move promotingMove = NULL_MOVE;

private:
	// deselecting current piece
	inline void deselect() {
		vSelectedCell = { -1, -1 };
		vAvailMoves.clear();
	}

	inline void drawPiece(Piece piece, olc::vf2d pos, olc::vf2d size) 
	{
		drawPiece(piece.getType(), piece.getColor(), pos, size);
	}

	inline void drawPiece(PieceType piece, Color color, olc::vf2d pos, olc::vf2d size)
	{
		if (piece != PieceType::NONE)
			drawPiece((float)piece, color, pos, size);
	}

	inline void drawPiece(float pieceType, Color color, olc::vf2d pos, olc::vf2d size)
	{
		olc::vf2d sourcePos{
			pieceType,
			color == Color::WHITE ? 0.0f : 1.0f
		};

		DrawPartialDecal(pos, size, dPieces.get(), sourcePos * sourceSize, sourceSize);
	}

	// 'to' as in convert
	inline int8_t screenToPromote(int x) {
		int relX = x - vPrompotingConsolePos.x;
		relX = std::clamp(relX, 0, vPrompotingConsoleSize.x - cellSize);

		return int8_t(relX / cellSize);
	}

	void afterMove(const Move& move)
	{
		if (board.isMated(board.getTurn()))
			bMate = true;

		if (bRecordMoves)
			moveRecord.addNewLine(move.str());
	}

	void doMove(const Move& move) {
		board.doMove(move);

		afterMove(move);


		vHoverCell = { -1, -1 };
		deselect();

		doComMove(move);

		bGameStarted = false;
	}

	void doComMove(const Move& playerMove)
	{
		if (bGameStarted)
			com.init(playerMove);
		else
			com.recordMove(playerMove, PLAYER_COLOR);

		board = com.doMove();
	}

	bool handleInput(float fElapsedTime) {
		UNUSED(fElapsedTime);

		if (GetKey(olc::ESCAPE).bPressed) return false;

		if (GetKey(olc::TAB).bPressed)
			ConsoleShow(olc::TAB, false);

		if (bPromoting) {
			hoverPromoteIndex = screenToPromote(GetMousePos().x);

			if (GetMouse(0).bPressed) {

				const PieceType type = (PieceType)(hoverPromoteIndex + (int8_t)PieceType::QUEEN);

				const auto move = std::find_if(vAvailMoves.begin(), vAvailMoves.end(),
					[&](const auto& move) { return move.to == promotingMove.to && move.promoteType == type; });

				assert(move != vAvailMoves.end() && "INTERNAL ERROR: have to find move");

				doMove(*move);

				bPromoting = false;
			}

			return true;
		}

		const olc::vt2d vMousePos = olc::trans<int8_t>( GetMousePos() / cellSize );
		const Piece piece = board.getPiece(vMousePos);
		if (auto move = std::find_if(vAvailMoves.begin(), vAvailMoves.end(), 
				[&](const Move& move) { return move.to == vMousePos; });
			move != vAvailMoves.end())
		{
			vHoverCell = vMousePos;

			if (GetMouse(0).bPressed)
			{
				// check if trying to promote
				if (move->promoteType != PieceType::NONE) {
					bPromoting = true;
					promotingMove = *move;
					return true;
				}

				doMove(*move);
			}
		}
		else if (!piece.isNull() && inBoardBounds(vMousePos)) {
			if (piece.getColor() == board.getTurn()) {
				vHoverCell = vMousePos;

				if (GetMouse(0).bPressed) {
					vSelectedCell = vMousePos;

					vAvailMoves = board.getAvailMoves(vMousePos);
				}
			}
		}
		else if (GetMouse(0).bPressed) {
			deselect();
		}

		return true;
	}

	void handleDrawing(float fElapsedTime) {
		UNUSED(fElapsedTime);

		for (int8_t y=0; y < BOARD_SIZE; ++y)
			for (int8_t x=0; x < BOARD_SIZE; ++x) {
				olc::Pixel color = (x + y) % 2 == 0
					? BG_WHITE
					: BG_BLACK;
				
				FillRect(x * cellSize, y * cellSize, cellSize, cellSize, color);

				Piece currPiece = board.getPiece(Pos(x, y));
				drawPiece(currPiece, olc::vf2d{ float(x * cellSize), float(y * cellSize) },
					olc::vf2d{ float(cellSize), float(cellSize) });
			}

		for (const auto& move : vAvailMoves) {
			// here so the promote square wouldn't be a bright green
			if (move.promoteType == PieceType::NONE || move.promoteType == PieceType::QUEEN)
				FillRectDecal(olc::trans<int>(move.to.toVec()) * cellSize, olc::vi2d{ (int)cellSize, (int)cellSize }, COL_AVAIL);
		}

		// TODO: show king in red if it is in a check

		FillRect(vSelectedCell.x * cellSize, vSelectedCell.y * cellSize, cellSize, cellSize, COL_SELECTED);
		DrawRect(vHoverCell.x * cellSize, vHoverCell.y * cellSize, cellSize - 1, cellSize - 1, COL_HOVER);

		// DRAW X-Y INDECIS
		
		// TODO: make DrawStringDecalShadow method
		// TODO: make this togellable via command
		for (int8_t x = 0; x < BOARD_SIZE; x++) {
			char c = 'A' + x;
			std::string s(1, c);
			const int8_t FONT_SIZE = 8;
			olc::vf2d pos( (x + 1) * (float)cellSize - FONT_SIZE, (float)(ScreenHeight() - FONT_SIZE));
			DrawStringDecal(pos + olc::vf2d{1,1}, s, olc::BLACK);
			DrawStringDecal(pos, s, olc::YELLOW);
		}
		for (int8_t y = 0; y < BOARD_SIZE; y++) {
			std::string s = std::to_string(BOARD_SIZE - y);
			DrawStringDecal(olc::vf2d{ 0, 1 } * y * (float)cellSize + olc::vf2d{1,1}, s, olc::BLACK);
			DrawStringDecal(olc::vf2d{ 0, 1 } * y * (float)cellSize, s, olc::YELLOW);
		}

		if (bPromoting) {
			FillRect(vPrompotingConsolePos, vPrompotingConsoleSize);

			for (int8_t pieceType = (int8_t)PieceType::QUEEN; pieceType < (int8_t)PieceType::PAWN; pieceType++) {
				olc::vi2d pos = vPrompotingConsolePos;
				pos.x += (pieceType - (int8_t)PieceType::QUEEN) * cellSize;
				drawPiece(pieceType, board.getTurn(), pos, olc::vf2d{ (float)cellSize, (float)cellSize });
			}

			DrawRect(vPrompotingConsolePos + olc::vi2d{ hoverPromoteIndex * cellSize, 0 }, olc::vi2d{cellSize, cellSize}, COL_HOVER);
		}

		if (bMate) {
			DrawRectDecal(olc::vf2d{ 0.0f, 0.0f }, olc::trans<float>(GetScreenSize()), olc::PixelF(0, 0, 0, 0.3f));
			DrawStringDecal(
				olc::vf2d{ 0.0f, 0.0f },
				"Mate On " + std::string(magic_enum::enum_name(board.getTurn()))
			);
		}
	}

	bool frameUpdate(float fElapsedTime) {
		// INPUT
		if (!IsConsoleShowing() && !bMate) {
			if (!handleInput(fElapsedTime))
				return false;
		}

		// DRAWING
		handleDrawing(fElapsedTime);
		return true;
	}

public:
	bool OnUserCreate() override
	{
		cellSize = std::min(ScreenWidth(), ScreenHeight()) / BOARD_SIZE;

		dPieces = std::make_unique<olc::Decal>(new olc::Sprite("./Pieces.png"));

		sourceSize = olc::vf2d{ (float)dPieces->sprite->width / PIECES_COUNT,
								(float)dPieces->sprite->width / PIECES_COUNT };

		moveRecord.setFileName("moves.log");

		vPrompotingConsoleSize = { 4 * cellSize, cellSize };
		vPrompotingConsolePos = (GetScreenSize() - vPrompotingConsoleSize) / 2;

		init();

		return true;
	}

	void init() {
		moveRecord.clear();

		board.init();
		com = Com(board);

		deselect();
		bMate = false;
		bPromoting = false;
		bGameStarted = true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		auto frameStart = std::chrono::high_resolution_clock::now();

		bool bContinue = frameUpdate(fElapsedTime);

		auto frameEnd = std::chrono::high_resolution_clock::now();
		double frameTime = (double)std::chrono::duration_cast<std::chrono::seconds>(frameEnd - frameStart).count();
		sleep_for(1.0 / MAX_FPS - frameTime);

		return bContinue;
	}

	bool OnUserDestroy() override
	{
		board.clearPieces();

		return true;
	}

	bool OnConsoleCommand(const std::string& sText) override
	{
		ConsoleCaptureStdOut(true);

		const auto unknownCommand = [](std::string text) {
			std::cout << "Error: unknown command - " << text << '\n';  
		};

		// TODO: make this method BP, with vector of the words
		//		 and printing right

		std::stringstream ss;
		ss << sText;

		std::string c1;
		ss >> c1;

		if (c1 == "reset") {
			board = Board();
			init();
			std::cout << "resetting board...\n";
		}
		else if (c1 == "get") {
			std::string c2;
			ss >> c2;

			if (c2 == "color") {
				std::cout << "Playing Color: " << magic_enum::enum_name(board.getTurn()) << '\n';
			}
			else if (c2 == "pawns") {
				OnConsoleCommand("get isolated");
				OnConsoleCommand("get doubled");
				OnConsoleCommand("get blocked");
			}
			else if (c2 == "isolated") {
				const auto [whitePawns, blackPawns] = board.getIsolatedPawns();
				std::cout << "Isolated pawns: white = " << (int16_t)whitePawns  
					<< ", black = " << (int16_t)blackPawns << '\n';
			}
			else if (c2 == "doubled") {
				const auto [whitePawns, blackPawns] = board.getDoubledPawns();
				std::cout << "Doubled pawns:  white = " << (int16_t)whitePawns  
					<< ", black = " << (int16_t)blackPawns << '\n';
			}
			else if (c2 == "blocked") {
				const auto [whitePawns, blackPawns] = board.getBlockedPawns();
				std::cout << "Blocked pawns:  white = " << (int16_t)whitePawns  
					<< ", black = " << (int16_t)blackPawns << '\n';
			}
			else if (c2 == "fen") {
				std::cout << "FEN: " << board.fen() << '\n';
			}
			else unknownCommand(c1 + " " + c2);
		}
		else if (c1 == "set") {
			std::string c2;
			ss >> c2;

			if (c2 == "turn" || c2 == "color") {
				std::string c3;
				ss >> c3;

				std::transform(c3.begin(), c3.end(), c3.begin(),
					[](unsigned char c) { return std::toupper(c); });

				auto color = magic_enum::enum_cast<Color>(c3);
				if (color.has_value()) {
					board.setTurn(color.value());
					std::cout << "Setting turn to: " << c3 << "\n";
				}
				else unknownCommand(c1 + " " + c2 + " " + c3);
			}
			else if (c2 == "fen") {
				// TODO: FIX: when parsing a position when it's blacks move. the computer thinks he's white
				board.setFen(ss);
				com.setBoard(board);
			}
			else unknownCommand(c1 + " " + c2);
		}
		else if (c1 == "isCheck") {
			std::string c2;
			ss >> c2;

			std::transform(c2.begin(), c2.end(), c2.begin(),
				[](unsigned char c) { return std::toupper(c); });

			auto color = magic_enum::enum_cast<Color>(c2);
			if (color.has_value()) {
				if (board.isChecked(color.value())) 
					std::cout << "Check on ";
				else std::cout << "No check on ";

				std::cout << c2 << '\n';
			}
			else unknownCommand(c1 + " " + c2);
		}
		else if (c1 == "print") {
			std::string c2;
			ss >> c2;
			
			if (c2 == "board") {
				std::cout << "Printing board to console\n";
				ConsoleCaptureStdOut(false);
				std::cout << board.str() << '\n';
				ConsoleCaptureStdOut(true);
			}
			else if (c2 == "fen") {
				std::cout << "Printing FEN to console\n";
				ConsoleCaptureStdOut(false);
				std::cout << "FEN: " << board.fen() << '\n';
				ConsoleCaptureStdOut(true);
			}
			else if (c2 == "availMoves") {
				std::cout << "Printing availMoves of location " << vSelectedCell.str() << " to console\n";

				ConsoleCaptureStdOut(false);
				printVector(vAvailMoves);
				ConsoleCaptureStdOut(false);
			}
			else unknownCommand(c1 + " " + c2);
		}
		else if (c1 == "record") {
			std::string c2;
			ss >> c2;

			if (c2 == "moves") {
				bRecordMoves = !bRecordMoves;
				std::cout << (bRecordMoves ? "Recording moves: they'll be in " + moveRecord.fileName() : "Stopped recording moves") << "\n";
			}
			else unknownCommand(c1 + " " + c2);
		}
		else unknownCommand(c1);
		

		ConsoleCaptureStdOut(false);
		return true;
	}
};

int main()
{
	Example demo;
	if (demo.Construct(256, 240, 4, 4))
		demo.Start();

	return 0;
}
