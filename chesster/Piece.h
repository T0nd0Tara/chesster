#pragma once
#include <pch.h>
#include "./common.h"
#include "./Move.h"
#include "./Pos.h"

#define NULL_PIECE Piece()

class Board;

struct Piece
{
	Piece()
		: Piece(PieceType::NONE, Color::WHITE)
	{}

	Piece(PieceType type, Color color)
		: type(type)
		, color(color)
	{}

	Piece(const Piece&) = default;

	inline PieceType getType() const
	{
		return type;
	}


	std::vector<Move> getMoves(Board board, Pos selfPos) const; 

	bool hasAnyAvailMove(Board board, Pos selfPos) const;

	inline char str() const
	{
		return typeToStr(getType(), getColor());
	}

	inline Color getColor() const
	{
		return color;
	}

	inline bool opesiteColor(const Piece* piece) const
	{
		if (piece == nullptr)
			return true;

		return color != piece->getColor();
	}

	std::vector<Move> getSeen(Board board, Pos selfPos) const;

	inline bool isNull() const
	{
		return type == PieceType::NONE;
	}

	static inline Piece create(PieceType type, Color color)
	{
		return Piece(type, color);
	}

	static inline Piece fromChar(const char c) {
		PieceType type;

		switch (tolower(c))
		{
		case 'r':
			type = PieceType::ROOK;
			break;
		case 'n':
			type = PieceType::KNIGHT;
			break;
		case 'b':
			type = PieceType::BISHOP;
			break;
		case 'p':
			type = PieceType::PAWN;
			break;
		case 'q':
			type = PieceType::QUEEN;
			break;
		case 'k':
			type = PieceType::KING;
			break;
		default:
			UNREACHABLE(std::format("Can't make piece of type: {}", c).c_str());
		}

		return Piece::create(type, isupper(c) ? Color::WHITE : Color::BLACK);
	}

public:
	bool operator==(const Piece& rhs) const
	{
		if (type == rhs.getType() && type == PieceType::NONE) {
			WARNING("SHOULD CHECK NULLABILITY OF A PIECE VIA Piece::isNull");
			return true;
		}

		return type == rhs.getType() && color == rhs.getColor();
	}
	bool operator!=(const Piece& rhs) const {
		return !(*this == rhs);
	}

	PieceType type : 3;
	Color color : 1;
};
