#pragma once
#include <pch.h>
#include "common.h"

#include "./Move.h"
#include "./Pos.h"

// struct Piece;
#include "Piece.h"

class Board
{
public:
	enum class CastleAbility : uint8_t
	{
		NONE  = 0b00,
		KING  = 0b01,
		QUEEN = 0b10,
		BOTH  = 0b11,
	};
	friend inline CastleAbility operator|(CastleAbility a, CastleAbility b) {
		return (CastleAbility)((uint8_t)a | (uint8_t)b);
	}
	friend inline CastleAbility operator&(CastleAbility a, CastleAbility b) {
		return (CastleAbility)((uint8_t)a & (uint8_t)b);
	}

public:
	Board();

	Board(const Board&) = default;

	std::string str() const;
	std::string fen() const;

	void setFen(std::stringstream& fen);

	inline Piece getPiece(int8_t i) const;
	inline Piece getPiece(Pos pos) const;
	
	void clear();
	void clearPieces();

	void init();
	void initKings();

	std::vector<Move> getAvailMoves(Pos pos) const;
	std::vector<Move> getAvailMoves(Color color) const;

	void doMove(const Move move, bool assertMove = true, bool changeTurn = true);

	inline Color getTurn() const {
		return m_turn;
	}

	inline void setTurn(Color turn) {
		m_turn = turn;
	}

	inline auto getCastleBlack() const {
		return m_castleBlack;
	}

	inline auto getCastleWhite() const {
		return m_castleWhite;
	}

	inline auto getUnPassantPos() const {
		return m_unPassantPos;
	}

	bool canCastle(CastleAbility, Color) const;
	void removeCastleAbility(CastleAbility, Color);
	void addCastleAbility(CastleAbility, Color);
	void clearCastle();

	bool isChecked(Color dangeredKing) const;
	bool isMated(Color dangeredKing);

	bool isMoveCheck(const Move move) const;

	// get all occurrences the pieceType
	std::vector<std::pair<Piece, Pos>> getType(PieceType) const;
	Pos getPos(const Piece) const;

	std::tuple<int8_t, int8_t> getDoubledPawns() const;
	std::tuple<int8_t, int8_t> getIsolatedPawns() const;
	std::tuple<int8_t, int8_t> getBlockedPawns() const;

public:
	inline void clearPiece(int8_t i);
	inline void clearPiece(Pos pos);
	inline void clearPiece(int8_t x, int8_t y);

	inline void setPiece(Piece piece, int8_t i);
	inline void setPiece(Piece piece, Pos pos);
	void setPiece(Piece piece, int8_t x, int8_t y);

public:
	bool operator==(const Board&) const = default;
	Board& operator=(const Board&) = default;
	friend inline std::ostream& operator<<(std::ostream& os, const Board& board)
	{
		return os << board.str();
	}

private:
	// each Piece is 4 bits, which mean we can store 2 in 1 byte
	// so each 2 grid places (in the X direction) 
	// are located in the Low-High parts of the byte
	// ex: Piece 0,0 are located in m_pieces[0][0] in the low part
	// see method Board::getPiece
	Piece m_pieces[BOARD_SIZE][BOARD_SIZE / 2];

	Color m_turn : 1;

	CastleAbility m_castleWhite : 2;
	CastleAbility m_castleBlack : 2;

	Pos m_unPassantPos = NULL_POS;
};
