#pragma once
#include <pch.h>

#include <olcPixelGameEngine.h>

#include <magic_enum.hpp>

#define FILE_POS             std::format("{}:{}:{}", __FILE__, __FUNCTION__, __LINE__)
#define WARNING(msg)         std::cout << "\x1B[33mWARNING(" << FILE_POS << "): " << (msg) << "\033[0m\n"
#define NOT_IMPLEMENTED(msg) WARNING(std::format("NOT IMPLEMENTED: {}", (msg)))
#define PRINT_ERROR(msg)	 std::cout << "\x1B[31mERROR("   << FILE_POS << "): " << (msg) << "\033[0m\n"
#define UNREACHABLE(msg)	 assert(false && "UNREACHABLE: " && (msg))

#define BOARD_SIZE (8)
#define PIECES_COUNT (6.0f)

#define MAX_FPS (30.0)

#define COM_COLOR (Color::BLACK)
#define PLAYER_COLOR (Color::WHITE)

typedef int_least16_t eval_t;
#define EVAL_MAX (INT_LEAST16_MAX)
#define EVAL_MIN (INT_LEAST16_MIN)

// 't' stand for tile or tiny
namespace olc {
	typedef v2d_generic<int8_t> vt2d;

	template<typename To, typename From>
	inline v2d_generic<To>	trans(v2d_generic<From> vec) {
		return v2d_generic<To>{ (To)vec.x, (To)vec.y };
	}
}

inline void sleep_for(double dt)	
{
	static constexpr std::chrono::duration<double> MinSleepDuration(0);
	auto start = std::chrono::high_resolution_clock::now();
	while (std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - start).count() < dt) {
		std::this_thread::sleep_for(MinSleepDuration);
	}
}

template<typename T>
inline void printVector(const std::vector<T>& vec) {
	std::cout << "{\n";
	
	for (const auto& item : vec) {
		std::cout << '\t' << item << '\n';
	}

	std::cout << "}\n";
}

inline bool inBoardBounds(int8_t x, int8_t y) {
	return x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE;
}

inline bool inBoardBounds(int8_t i) {
	return i >= 0 && i < BOARD_SIZE* BOARD_SIZE;
}


enum class Color : uint8_t {
	WHITE,
	BLACK,
};

enum class PieceType : uint8_t {
	// DON'T CHANGE ORDER!!!
	// it's not BP but fuck it
	NONE = 7,
	KING = 0,
	QUEEN,
	BISHOP,
	KNIGHT,
	ROOK,
	PAWN,
};

enum SquareNotation : int8_t
{
	a8, b8, c8, d8, e8, f8, g8, h8,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a1, b1, c1, d1, e1, f1, g1, h1,
};



inline char typeToStr(PieceType type, Color color = Color::WHITE)
{
	char out = ' ';
	switch (type)
	{
	case PieceType::KING:
		out = 'K';
		break;
	case PieceType::QUEEN:
		out = 'Q';
		break;
	case PieceType::BISHOP:
		out = 'B';
		break;
	case PieceType::KNIGHT:
		out = 'N';
		break;
	case PieceType::ROOK:
		out = 'R';
		break;
	case PieceType::PAWN:
		out = 'P';
		break;
	default:
		return out;
	}

	if (color == Color::BLACK)
		out += 'a' - 'A';

	return out;
};
