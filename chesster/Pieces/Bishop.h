#pragma once
#include "Piece.h"

class Bishop : public Piece
{
public:
	Bishop(Color color, uint8_t moves = 0) : Piece(color, moves) {}

	inline Bishop* clone() const
	{
		return new Bishop(*this);
	}

	inline PieceType getType() const
	{
		return PieceType::BISHOP;
	}

	std::vector<Move> getSeen(const Board* board, Pos selfPos) const 
	{
		std::vector<Move> out;

		const auto min = [](int8_t a, int8_t b) { return a < b ? a : b; };

		// TODO: move this method to Piece, as 3 Pieces use it
		const auto addMoves = [&](int maxIteration, Pos diffMove)
		{
			for (int8_t i = 1; i <= maxIteration; ++i) {
				Pos availPos = selfPos + (i * diffMove);
				if (!inBoardBounds(availPos))
					continue;

				const Piece* piece = board->getPiece(availPos);
				
				if (piece != nullptr) {
					if (opesiteColor(piece))
						out.emplace_back(selfPos, availPos, availPos);

					return;
				}

				out.emplace_back(selfPos, availPos);
			}
		};

		// NW
		addMoves(min(selfPos.x(), selfPos.y()), Pos{-1, -1});

		// NE
		addMoves(min(BOARD_SIZE - selfPos.x() - 1, selfPos.y()), Pos{1, -1});

		// SW
		addMoves(min(selfPos.x(), BOARD_SIZE - selfPos.y() - 1), Pos{ -1, 1 });

		// SE
		addMoves(min(BOARD_SIZE - selfPos.x(), BOARD_SIZE - selfPos.y()) - 1, Pos{ 1, 1 });

		return out;
	}
};
