#pragma once
#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(Color color, uint8_t moves = 0) : Piece(color, moves) {}

	inline Knight* clone() const
	{
		return new Knight(*this);
	}

	inline PieceType getType() const
	{
		return PieceType::KNIGHT;
	}

	std::vector<Move> getSeen(const Board* board, Pos selfPos) const
	{
		std::vector<Move> out;

		// TODO: improve performance
		std::vector<olc::vt2d> moveDiff = {
			{1, 2}, {2, 1},
			{-1, 2}, {2, -1},
			{1, -2}, {-2, 1},
			{-1, -2}, {-2, -1}
		};

		for (olc::vt2d diff : moveDiff) {
			// we use olc::vt2d instead of Pos, because Pos make the moves wrap around the screen
			olc::vt2d availPos = selfPos.toVec() + diff;
			if (!inBoardBounds(availPos.x, availPos.y)) continue;

			const Piece* eatingPiece = board->getPiece(availPos);
			if (opesiteColor(eatingPiece))
				out.emplace_back(selfPos, availPos, eatingPiece == nullptr ? NULL_POS : availPos);
		}

		return out;
	}


};
