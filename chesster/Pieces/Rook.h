#pragma once
#include "Piece.h"

class Rook : public Piece
{
public:
	Rook(Color color, uint8_t moves = 0) : Piece(color, moves) {}

	inline Rook* clone() const
	{
		return new Rook(*this);
	}

	inline PieceType getType() const
	{
		return PieceType::ROOK;
	}

	std::vector<Move> getSeen(const Board* board, Pos selfPos) const 
	{
		std::vector<Move> out;

		const auto addMoves = [&](int8_t maxIteration, Pos diffMove)
		{
			for (int8_t i = 1; i <= maxIteration; ++i) {
				Pos availPos = selfPos + (i * diffMove);
				if (!inBoardBounds(availPos))
					continue;

				const Piece* piece = board->getPiece(availPos);
				
				if (piece != nullptr) {
					if (opesiteColor(piece))
						out.emplace_back(selfPos, availPos, availPos);

					return;
				}

				out.emplace_back(selfPos, availPos);
			}
		};

		// N
		addMoves(selfPos.y(), Pos{0, -1});

		// S
		addMoves(BOARD_SIZE - selfPos.y() - 1, Pos{0, 1});

		// W
		addMoves(selfPos.x(), Pos{-1, 0});

		// E
		addMoves(BOARD_SIZE - selfPos.x() - 1, Pos{1, 0});

		return out;
	}
};

