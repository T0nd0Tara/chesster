#pragma once
#include "Piece.h"


class Pawn : public Piece
{
public:
	Pawn(Color color, uint8_t moves = 0) : Piece(color, moves) {}
	
	inline Pawn* clone() const
	{
		return new Pawn(*this);
	}

	inline PieceType getType() const
	{
		return PieceType::PAWN;
	}

	std::vector<Move> getSeen(const Board* board, Pos selfPos) const
	{
		std::vector<Move> out;
		int8_t nDir = m_color == Color::WHITE ? -1 : 1;

		Pos oneAhead{ selfPos + olc::vt2d{0, nDir * 1} };
		const auto putMove = [&](Pos to, Pos eat = NULL_POS) {
			if (!inBoardBounds(to)) return;
			if (!inBoardBounds(eat) && eat != NULL_POS) return;

			if (oneAhead.y() != 0 && oneAhead.y() != BOARD_SIZE - 1) {
				out.emplace_back(selfPos, to, eat);
			}
			else {
				for (int8_t pieceType = (int8_t)PieceType::KING + 1; pieceType < (int8_t)PieceType::PAWN; pieceType++) {
					out.emplace_back(selfPos, to, eat,
						magic_enum::enum_cast<PieceType>(pieceType).value());
				}
			}
		};
		
		if (board->getPiece(oneAhead) == nullptr) {
			putMove(oneAhead);

			if (Pos twoAhead{ selfPos + olc::vt2d{0, nDir * 2} };
				board->getPiece(twoAhead) == nullptr && m_nMoves == 0)
				putMove(twoAhead);
		}

		{
			// eat right
			Pos availPos{ selfPos + olc::vt2d{1, nDir} };
			const Piece* piece = board->getPiece(availPos);
			if (piece != nullptr && inBoardBounds(availPos)) {
				if (opesiteColor(piece))
					putMove(availPos, availPos);
			}
			// en passant
			else if (Pos enPassantPos{ selfPos.x() + 1, selfPos.y()};
					enPassantPos == board->getLastPos() && (selfPos.y() == 3 || selfPos.y() == 4)) {
				const Piece* enPassant = board->getPiece(enPassantPos);
				if (dynamic_cast<Pawn*>((Piece*)enPassant) != nullptr)
					if (opesiteColor(enPassant) && enPassant->getNumMoves() == 1)
						putMove(availPos, enPassantPos);
			}
		}

		{
			// eat left
			Pos availPos{ selfPos + olc::vt2d{-1, nDir} };
			const Piece* piece = board->getPiece(availPos);
			if (piece != nullptr && inBoardBounds(availPos)) {
				if (opesiteColor(piece))
					putMove(availPos, availPos);
			}
			// en passant
			else if (Pos enPassantPos{ selfPos.x() - 1, selfPos.y()}; 
				enPassantPos == board->getLastPos() && (selfPos.y() == 3 || selfPos.y() == 4)) {
				const Piece* enPassant = board->getPiece(enPassantPos);
				if (dynamic_cast<Pawn*>((Piece*)enPassant) != nullptr)
					if (opesiteColor(enPassant) && enPassant->getNumMoves() == 1)
						putMove(availPos, enPassantPos);
			}
		}

		return out;
	}
};

