#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	King(Color color, uint8_t moves = 0) : Piece(color, moves) {}

	inline King* clone() const
	{
		return new King(*this);
	}

	inline PieceType getType() const
	{
		return PieceType::KING;
	}

	std::vector<Move> getMoves(Board* board, Pos selfPos) const override {
		std::vector<Move> moves = getSeen(board, selfPos);

		moves.erase(std::remove_if(moves.begin(), moves.end(), [&](const Move& move) {
			return board->isMoveCheck(move);
		}), moves.end());

		if (!board->isChecked(m_color))
			addCastleMoves(board, selfPos, moves);

		return moves;
	}


	std::vector<Move> getSeen(const Board* board, Pos selfPos) const
	{
		std::vector<Move> out;

		for (int8_t y = -1; y <= 1; y++)
			for (int8_t x = -1; x <= 1; x++) {
				olc::vt2d availPos{ selfPos.x() + x, selfPos.y() + y};
				if (!inBoardBounds(availPos.x, availPos.y) || Pos(availPos) == selfPos)
					continue;

				const Piece* eatingPiece = board->getPiece(availPos);
				if (opesiteColor(eatingPiece))
					out.emplace_back(selfPos, availPos, eatingPiece == nullptr ? NULL_POS : availPos);
			}

		return out;
	}

private:
	void addCastleMoves(const Board* board, Pos selfPos, std::vector<Move>& out) const {
		if (getNumMoves() != 0) return;

		{
			// left castle (long)
			const Piece* rook = board->getPiece(Pos{0, selfPos.y()});
			
			// we don't need to check if it is actually a rook, because we check it has never moved
			if (rook != nullptr) 
				if (rook->getNumMoves() == 0)
				{
					bool bAbleToCasle = true;
					Board tempBoard(board);

					for (int8_t posX = selfPos.x() - 1; posX >= selfPos.x() - 2; posX--) {
						if (board->getPiece(Pos{ posX, selfPos.y()}) != nullptr) {
							bAbleToCasle = false;
							break;
						}

						tempBoard.doMove(Move(Pos{ posX + 1, selfPos.y() }, Pos{ posX, selfPos.y() }), false);

						if (tempBoard.isChecked(getColor())) {
							bAbleToCasle = false;
							break;
						}
					}

					if (bAbleToCasle)
						out.push_back(Move{ selfPos, Pos{selfPos.x() - 2, selfPos.y()}});
				}
		
		}
		{

			// right castle (long)
			const Piece* rook = board->getPiece(Pos{ BOARD_SIZE - 1, selfPos.y() });

			// we don't need to check if it is actually a rook, because we check it has never moved
			if (rook != nullptr)
				if (rook->getNumMoves() == 0)
				{
					bool bAbleToCasle = true;
					Board tempBoard(board);

					for (int8_t posX = selfPos.x() + 1; posX <= selfPos.x() + 2; posX++) {
						if (board->getPiece(Pos{ posX, selfPos.y() }) != nullptr) {
							bAbleToCasle = false;
							break;
						}
						tempBoard.doMove(Move(Pos{ posX - 1, selfPos.y() }, Pos{ posX, selfPos.y() }), false);

						if (tempBoard.isChecked(getColor())) {
							bAbleToCasle = false;
							break;
						}
					}

					if (bAbleToCasle)
						out.push_back(Move{ selfPos, Pos{selfPos.x() + 2, selfPos.y()} });
				}
		}
		
	}
};

