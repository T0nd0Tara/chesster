#pragma once
#include "pch.h"

class FileHandler {
	std::string m_sFileName;

public:
	inline bool clear() {
		std::ofstream f(m_sFileName, std::ofstream::trunc);
		if (!f.is_open())
			return false;
		
		f.close();
		return true;
	}
	inline bool addNewLine(std::string input) {
		std::ofstream f(m_sFileName, std::ios::app);
		if (!f.is_open())
			return false;
		f << input << '\n';
		f.close();

		return true;
	}

	inline std::string fileName() const {
		return m_sFileName;
	}

	inline void setFileName(std::string fileName) {
		m_sFileName = fileName;
	}
};
