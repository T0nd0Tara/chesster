#pragma once
#include "./common.h"
#include "./Pos.h"

#define NULL_MOVE Move()

// TODO: see if I can remove the 'from' dm from the class
//		 because in most cases its pretty obvious
struct Move {
	Move()
		: Move(NULL_POS, NULL_POS)
	{
	}

	explicit Move(Pos from_, Pos to_, Pos eatPos_ = NULL_POS, PieceType promoteType_ = PieceType::NONE)
		: from(from_), to(to_), eatPos(eatPos_), promoteType(promoteType_)
	{ 
	}

	Pos from;
	Pos to;

	Pos eatPos;

	PieceType promoteType;

	inline std::string str() const {
		std::string out = "Move(Pos" + from.toVec().str() + ", Pos" + to.toVec().str();
		if (eatPos != NULL_POS) out += ", Pos" + eatPos.toVec().str();
		if (promoteType != PieceType::NONE) out += ", promote=PieceType::" + std::string(magic_enum::enum_name(promoteType));
		out += ")";
		return out;
	}

	std::string algebraicNotation() const {
		std::string out = (eatPos == NULL_POS) ? "" : "x";
		out += from.algebraicNotation();
		out += "-";
		out += to.algebraicNotation();

		if (promoteType != PieceType::NONE)
			typeToStr(promoteType);

		return out;

	}

	bool operator==(const Move&) const = default;

	friend inline std::ostream& operator << (std::ostream& stream, const Move& move) {
		return stream << move.str();
	}
};
