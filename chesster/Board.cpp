#include <pch.h>
#include "Board.h"
#include "./Piece.h"
#include "./common.h"
#include <cctype>

// CONSTRUCTORS / DESTRUCTOR
Board::Board()  
	: m_turn(Color::WHITE)  
	, m_castleWhite(CastleAbility::NONE)  
	, m_castleBlack(CastleAbility::NONE)  
{
	clearPieces();
}

// METHODS

std::string Board::str() const
{
	std::string out = "{\n";

	for (int8_t y = 0; y < BOARD_SIZE; y++)
	{
		out += '\t';

		for (int8_t x = 0; x < BOARD_SIZE; x++) {
			const Piece piece = getPiece(Pos(x, y));

			out += piece.str();
			out += ' ';
		}
		out += '\n';
	}

	return out + "}";
}

std::string Board::fen() const
{
	std::string out = "";

	for (int8_t y = 0; y < BOARD_SIZE; y++) {
		int8_t empty = 0;
		for (int8_t x = 0; x < BOARD_SIZE; x++) {
			const Piece piece = getPiece(Pos{ x, y });

			if (piece.isNull()) {
				empty++;
			}
			else {
				if (empty > 0) {
					out += std::to_string(empty);
					empty = 0;
				}

				out += piece.str();
			}
		}

		if (empty > 0) {
			out += std::to_string(empty);
		}

		if (y != BOARD_SIZE - 1) {
			out += "/";
		}
	}

	out += (m_turn == Color::WHITE) ? " w " : " b ";

	const auto str = [](CastleAbility ability, Color color) {
		std::string out;
		if ((uint8_t)(ability & CastleAbility::KING)) {
			out += typeToStr(PieceType::KING, color);
		}
		if ((uint8_t)(ability & CastleAbility::QUEEN)) {
			out += typeToStr(PieceType::QUEEN, color);
		}

		return out;
	};
	out += str(m_castleWhite, Color::WHITE) + str(m_castleBlack, Color::BLACK) + " ";

	out += (m_unPassantPos == NULL_POS) ? "-" : m_unPassantPos.algebraicNotation();

	return out;
}

void Board::setFen(std::stringstream& fen) {
	clear();

	std::string board;
	fen >> board;

	
	for (int8_t index = 0, x = 0, y = 0; index < board.size(); index++){
		char c = board[index];
		if (c == '/') {
			if (x != BOARD_SIZE) {
				PRINT_ERROR(std::format("char at position col={}, row={}. Is '/', which it cannot be\n", (int)x, (int)y));
				clear();
				return;
			}

			x = 0;
			y++;
		} else if (isdigit(c)) {
			x += c - '0';
		} else {
			Piece piece = Piece::fromChar(c);
			setPiece(piece, Pos(x, y));
			x++;
		}
	}

	std::string color;
	fen >> color;
	m_turn = color == "w" ? Color::WHITE : Color::BLACK;

	std::string castle;
	fen >> castle;

	for (char castleType : castle) {
		Color color = isupper(castleType) ? Color::WHITE : Color::BLACK;

		if (tolower(castleType) == 'k') {
			addCastleAbility(CastleAbility::KING, color);
		}
		else{
			assert(tolower(castleType) == 'q');
			addCastleAbility(CastleAbility::QUEEN, color);
		}
	}

	std::string unpassantMove;
	fen >> unpassantMove;
	m_unPassantPos = unpassantMove == "-" ? NULL_POS : Pos(magic_enum::enum_cast<SquareNotation>(unpassantMove).value());
}

inline Piece Board::getPiece(int8_t i) const
{
	return getPiece(Pos(i));
}

inline Piece Board::getPiece(Pos pos) const
{
	if (!inBoardBounds(pos)) return NULL_PIECE;

	uint8_t data = *((uint8_t*)&m_pieces[pos.y()][pos.x() / 2]);
	// equiv to: if (pos.x() % 2 == 1) data = data >> 4
	data >>= (4 * (pos.x() % 2));
	data &= 0xF;
	return *(Piece*)&data;
}

void Board::clear()
{
	clearPieces();
	clearCastle();
}

void Board::clearPieces()  
{
	for (int8_t y = 0; y < BOARD_SIZE; y++) {
		for (int8_t x = 0; x < BOARD_SIZE; x++) {
			clearPiece(x,y);
		}
	}
}

void Board::init(){
	clearPieces();
	// -- PAWNS --
	for (int8_t x = 0; x < BOARD_SIZE; x++)
	{
		setPiece(Piece::create(PieceType::PAWN, Color::WHITE), x, 6);
		setPiece(Piece::create(PieceType::PAWN, Color::BLACK), x, 1);
	}
	
	// -- BLACK PIECES --

	setPiece(Piece::create(PieceType::ROOK, Color::BLACK), 0, 0);
	setPiece(Piece::create(PieceType::ROOK, Color::BLACK), 7, 0);

	setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), 1, 0);
	setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), 6, 0);

	setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), 2, 0);
	setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), 5, 0);

	setPiece(Piece::create(PieceType::QUEEN, Color::BLACK), 3, 0);

	// -- WHITE PIECES --

	setPiece(Piece::create(PieceType::ROOK, Color::WHITE), 0, 7);
	setPiece(Piece::create(PieceType::ROOK, Color::WHITE), 7, 7);

	setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 1, 7);
	setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 6, 7);

	setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), 2, 7);
	setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), 5, 7);

	setPiece(Piece::create(PieceType::QUEEN, Color::WHITE), 3, 7);

	// -- KINGS --
	initKings();

	// -- CASTLE ABILITY --
	m_castleWhite = CastleAbility::KING | CastleAbility::QUEEN;
	m_castleBlack = CastleAbility::KING | CastleAbility::QUEEN;
}

void Board::initKings()  
{
	setPiece(Piece::create(PieceType::KING, Color::BLACK), 4, 0);
	setPiece(Piece::create(PieceType::KING, Color::WHITE), 4, 7);
}

std::vector<Move> Board::getAvailMoves(Pos piecePos) const {
	const Piece piece = getPiece(piecePos);
	
	if (piece.isNull())
		return std::vector<Move>();

	return piece.getMoves(*this, piecePos);
}

std::vector<Move> Board::getAvailMoves(Color color) const {
	std::vector<Move> out;
	
	// TODO: FIXME: use metadata
	for (int8_t y = 0; y < BOARD_SIZE; y++)
		for (int8_t x = 0; x < BOARD_SIZE; x++)
		{
			const Pos pos(x, y);
			const Piece piece = getPiece(pos);

			if (piece.isNull()) continue;
			if (piece.getColor() != color) continue;
			
			std::vector<Move> pieceMoves = piece.getMoves(*this, pos);

			out.reserve(out.size() + pieceMoves.size());
			out.insert(out.end(), pieceMoves.begin(), pieceMoves.end());
		}

	return out;
}

// TODO: refactor
void Board::doMove(const Move move, bool assertTurn, bool changeTurn)
{
	Piece moving = getPiece(move.from);

	if (moving.isNull()) {
		PRINT_ERROR(std::format("UNABLE TO DO MOVE {}: piece to move is null",  move.str()));
		return;
	}

	const Color movingColor = moving.getColor();
	if (movingColor != m_turn && assertTurn) {
		PRINT_ERROR(std::format("UNABLE TO DO MOVE {}: turn is not yours", move.str()));
		return;
	}

	if (move.eatPos != NULL_POS) {
		clearPiece(move.eatPos);
	}

	// check un passant move
	m_unPassantPos = NULL_POS;
	if (moving.getType() == PieceType::PAWN) {
		int8_t diff = move.from.y() - move.to.y();

		if (std::abs(diff) == 2) {
			m_unPassantPos = Pos(move.to.x(), (move.from.y() + move.to.y()) / 2);
		}
	}

	clearPiece(move.from);
	if (move.promoteType != PieceType::NONE) {
		assert(moving.getType() == PieceType::PAWN && "INTERNAL ERROR: cannot promote non pawn piece");
		
		switch (move.promoteType) {
		case PieceType::KNIGHT:
		case PieceType::BISHOP:
		case PieceType::ROOK:
		case PieceType::QUEEN:
			break;
		default:
			UNREACHABLE("promoteType can't be this value");
		}

		moving = Piece::create(move.promoteType, movingColor);
	}  
	setPiece(moving, move.to);


	if (changeTurn)
		m_turn = moving.getColor() == Color::WHITE ? Color::BLACK : Color::WHITE;

	if (moving.getType() == PieceType::ROOK) {
		if (move.from == Pos(0,0) || move.from == Pos(0,BOARD_SIZE - 1))
			removeCastleAbility(CastleAbility::QUEEN, moving.getColor());
		else if (move.from == Pos(BOARD_SIZE - 1,0) || move.from == Pos(BOARD_SIZE - 1,BOARD_SIZE - 1))
			removeCastleAbility(CastleAbility::KING, moving.getColor());
	}
	else if (moving.getType() == PieceType::KING) {
		// remove castle
		removeCastleAbility(CastleAbility::KING | CastleAbility::QUEEN, moving.getColor());

		// check if it is a castle move
		int8_t to_x = move.to.x();
		int8_t from_x = move.from.x();

		// left castle (short)
		if (to_x == from_x - 2)
			doMove(Move{ Pos{0, move.from.y()}, Pos{to_x + 1, move.to.y()}}, false, false);
		// right castle (long)
		else if (to_x == from_x + 2)
			doMove(Move{ Pos{BOARD_SIZE - 1, move.from.y()}, Pos{to_x - 1, move.to.y()}}, false, false);
	}
}

bool Board::canCastle(CastleAbility side, Color color) const
{
	assert(side != CastleAbility::NONE && side != CastleAbility::BOTH);

	CastleAbility ability = color == Color::WHITE ? getCastleWhite() : getCastleBlack();

	if (!(bool)(ability & side))
		return false;

	const Pos kingPos(4, color == Color::BLACK ? 0 : BOARD_SIZE - 1);
	const int8_t dir = side == CastleAbility::KING ? 1 : -1;


	// TODO: connect the 2 'for' loops
	//		 NOTICE: if side == CastleAbility::QUEEN, the first for loop has 3 iterations while the other has 2

	// check if path empty
	for (int8_t x = dir; kingPos.x() + x > 0 && kingPos.x() + x < BOARD_SIZE - 1; x += dir)
	{
		if (!getPiece(kingPos + Pos{ x, 0}).isNull()) {
			return false;
		}
	}

	// check if there's a 'check' in the way
	Board tempBoard(*this);
	for (int8_t i = 0; i <= 2; i++) {
		if (tempBoard.isChecked(color)) {
			return false;
		}

		tempBoard.doMove(Move(kingPos + Pos(i * dir, 0), kingPos + Pos((i + 1) * dir, 0)), false);
	}

	return true;
}

void Board::removeCastleAbility(CastleAbility abilityToRemove, Color color)
{
	if (color == Color::WHITE) 
		m_castleWhite = m_castleWhite & (CastleAbility)(~(uint8_t)abilityToRemove);
	else
		m_castleBlack = m_castleBlack & (CastleAbility)(~(uint8_t)abilityToRemove);
}

void Board::addCastleAbility(CastleAbility ability, Color color)
{
	if (color == Color::WHITE) 
		m_castleWhite = m_castleWhite | ability;
	else
		m_castleBlack = m_castleBlack | ability;
	
}

void Board::clearCastle() {
	m_castleBlack = CastleAbility::NONE;
	m_castleWhite = CastleAbility::NONE;
}

bool Board::isChecked(Color dangeredKing) const {
	const Pos kingPos = getPos(Piece(PieceType::KING, dangeredKing));
	// TODO: maybe move this assert somewhere else to save time
	assert(inBoardBounds(kingPos) && "Invalid king position id Board::isChecked method");

	Board allegedBoard(*this);
	for (PieceType type : {PieceType::BISHOP, PieceType::KNIGHT, PieceType::QUEEN, PieceType::ROOK})
	{
		const Piece piece(type, dangeredKing);
		allegedBoard.setPiece(piece, kingPos);

		std::vector<Move> seeingPositions = piece.getSeen(allegedBoard, kingPos);

		if (std::find_if(seeingPositions.begin(), seeingPositions.end(),
			[allegedBoard, type](Move move) {
				return move.eatPos != NULL_POS &&
					allegedBoard.getPiece(move.eatPos).getType() == type;
			}) != seeingPositions.end())
			return true;
	}

	int8_t dir = dangeredKing == Color::BLACK ? -1 : 1;
	Piece allegedPawn;
	for (int8_t side : {-1, 1}) {
		allegedPawn = getPiece(kingPos + Pos(side, dir));
		if (allegedPawn.color != dangeredKing && allegedPawn.getType() == PieceType::PAWN)
			return true;
	}

	return false;
}

bool Board::isMated(Color dangeredKing) {
	const Pos kingPos = getPos(Piece(PieceType::KING, dangeredKing));

	// TODO: maybe move this assert somewhere else to save time
	assert(inBoardBounds(kingPos) && "Invalid king position id Board::isMated method");

	for (int8_t i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
		const Piece defendPiece = getPiece(i);

		if (defendPiece.isNull()) continue;
		if (defendPiece.getColor() != dangeredKing) continue;


		if (defendPiece.hasAnyAvailMove(*this, Pos(i))) 
			return false;
	}

	return true;
}

bool Board::isMoveCheck(const Move move) const
{
	Board allegedBoard(*this);

	allegedBoard.doMove(move, false, false);

	return allegedBoard.isChecked(allegedBoard.getPiece(move.to).getColor());
}

std::vector<std::pair<Piece, Pos>> Board::getType(PieceType type) const
{
	std::vector<std::pair<Piece, Pos>> out;

	// TODO: FIXME: use metadata
	for (int8_t y = 0; y < BOARD_SIZE; y++)
		for (int8_t x = 0; x < BOARD_SIZE; x++)
		{
			const Pos pos(x, y);
			Piece piece = getPiece(pos);

			if (piece.getType() == type)
				out.push_back(std::make_pair(piece, pos));
		}

	return out;
}

Pos Board::getPos(const Piece piece) const
{
	for (int8_t y = 0; y < BOARD_SIZE; y++)
		for (int8_t x = 0; x < BOARD_SIZE; x++)
		{
			Pos pos(x, y);
			if (getPiece(pos) == piece)
				return pos;
		}

	return NULL_POS;
}

std::tuple<int8_t, int8_t> Board::getDoubledPawns() const
{
	int8_t whitePawns = 0;
	int8_t blackPawns = 0;

	 std::vector<std::pair<Piece, Pos>> pawns = getType(PieceType::PAWN);

	// TODO: improve algorithm
	for (const auto pawn: pawns)  
	{
		const int8_t dir = pawn.first.getColor() == Color::WHITE ? -1 : 1;

		for (int8_t y = pawn.second.y() + dir; y >= 0 && y < BOARD_SIZE; y += dir)
		{
			Piece piece = getPiece(Pos{ pawn.second.x(), y });
			if (piece.isNull()) continue;

			if (piece.getType() == PieceType::PAWN &&
				piece.getColor() == pawn.first.getColor())
			{
				if (pawn.first.getColor() == Color::WHITE)
					whitePawns++;
				else
					blackPawns++;

				break;
			}
		}

	}

	return {whitePawns, blackPawns};
}

std::tuple<int8_t, int8_t> Board::getIsolatedPawns() const
{
	int8_t whitePawns = 0;
	int8_t blackPawns = 0;

	std::vector<std::pair<Piece, Pos>> pawns = getType(PieceType::PAWN);

	for (const auto pawn : pawns)
	{
		// TODO: remove goto without decreasing performance
		for (int8_t diffY = -1; diffY <= 1; diffY++) {
			for (int8_t diffX = -1; diffX <= 1; diffX++)
			{
				if (diffX == 0 && diffY == 0) continue;

				const Pos pos = pawn.second + Pos{ diffX, diffY };
				if (!inBoardBounds(pos)) continue;

				const Piece piece = getPiece(pos);

				if (piece.getColor() == pawn.first.getColor() &&
					piece.getType() == PieceType::PAWN)
					goto NOT_ISOLATED;
			}
		}
		
		if (pawn.first.getColor() == Color::WHITE)
			whitePawns++;
		else
			blackPawns++;

	NOT_ISOLATED: {}
	}

	return {whitePawns, blackPawns};
}

std::tuple<int8_t, int8_t> Board::getBlockedPawns() const
{
	int8_t whitePawns = 0;
	int8_t blackPawns = 0;

	std::vector<std::pair<Piece, Pos>> pawns = getType(PieceType::PAWN);

	for (const auto pawn: pawns)  
	{
		const int8_t dir = pawn.first.getColor() == Color::WHITE ? -1 : 1;

		{
			const Piece piece = getPiece(Pos{ pawn.second.x(), pawn.second.y() + dir});

			if (piece.isNull()) continue;
		}
		
		if (pawn.first.getColor() == Color::WHITE)
			whitePawns++;
		else
			blackPawns++;
	}

	return {whitePawns, blackPawns};

}


void inline Board::clearPiece(int8_t i) {
	clearPiece(Pos(i));
}

void inline Board::clearPiece(Pos pos) {
	clearPiece(pos.x(), pos.y());
}

void inline Board::clearPiece(int8_t x, int8_t y) {
	setPiece(NULL_PIECE, x, y);
}

void inline Board::setPiece(Piece piece, int8_t i)
{
	setPiece(piece, Pos(i));
}

void inline Board::setPiece(Piece piece, Pos pos) {
	setPiece(piece, pos.x(), pos.y());
}

void Board::setPiece(Piece piece, int8_t x, int8_t y)
{
	assert(inBoardBounds(x, y));

	uint8_t data      = *(uint8_t*)&m_pieces[y][x/2];
	uint8_t pieceData = *(uint8_t*)&piece;
	uint8_t shifting  = 4 * (x % 2);
	
	data &= 0xF0 >> shifting;
	data |= (pieceData & 0xF) << shifting;

	m_pieces[y][x / 2] = *(Piece*)&data;
}

