#include <pch.h>

#include "../common.h"
#include "Com.h"
#include "../Piece.h"
#include "../Board.h"

BS::thread_pool Com::pool = BS::thread_pool(THREADS_COUNT);

const eval_t Com::pieceValues[] = {
	2000,
	90,
	30,
	30,
	50,
	10
};

Com::Com(Board board)  
	: m_board(board)
{ }

void Com::init(Move firstMove)
{
	std::cout << "THREADS: " << THREADS_COUNT << "\n";

	m_board.doMove(firstMove);
	m_tree = std::make_unique<StateTree>(firstMove);
	m_tree->populate(m_board, COM_DEPTH);
	pool.wait_for_tasks();

	//std::ofstream f("tree.txt", std::ios::trunc);
	//f << m_tree->str() << '\n';
	//f.close();
}

// source: https://www.chessprogramming.org/Evaluation
// positive - current turn's favour
eval_t Com::evalBoard(Board board)
{
#if 0
	if (board.isMated(PLAYER_COLOR))
		return EVAL_MIN;

	if (board.isMated(COM_COLOR))
		return EVAL_MAX;
#endif


	// TODO: save where each Piece type is in Board
	//		 that way we don't need to go through each and every cell

	eval_t sum = 0;
	for (int8_t y = 0; y < BOARD_SIZE; y++)
		for (int8_t x = 0; x < BOARD_SIZE; x++)
		{
			const Pos pos(x, y);
			const Piece piece = board.getPiece(pos);
			if (piece.isNull()) continue;

			const eval_t coeff = piece.getColor() == COM_COLOR ? 1 : -1;

			sum += coeff * pieceValues[(int8_t)piece.getType()];
			sum += coeff * (eval_t)piece.getMoves(board, pos).size();
		}

	const auto [dPawnsWhite, dPawnsBlack] = board.getDoubledPawns();
	const auto [sPawnsWhite, sPawnsBlack] = board.getBlockedPawns();
	const auto [iPawnsWhite, iPawnsBlack] = board.getIsolatedPawns();

	const auto diffDPawns = dPawnsBlack - dPawnsWhite;
	const auto diffSPawns = sPawnsBlack - sPawnsWhite ;
	const auto diffIPawns = iPawnsBlack - iPawnsWhite;

	sum -= (eval_t)5 * (eval_t)(diffDPawns + diffSPawns + diffIPawns);

	return (board.getTurn() == COM_COLOR? 1 : -1 ) * sum;
}

void Com::recordMove(Move move, Color moveColor)
{
	std::cout << "tree root: " << m_tree->m_move.algebraicNotation() << '\n';
	const auto node = std::find_if(m_tree->m_children.begin(), m_tree->m_children.end(),
		[move](const auto& child) { return child->m_move == move; });

	assert(node != m_tree->m_children.end() && "Couldn't find that move");

	m_board.doMove((*node)->m_move, false);
	m_tree = std::move(*node);

	if (moveColor == PLAYER_COLOR) {
		auto start = std::chrono::high_resolution_clock::now();

		m_tree->populate(m_board, COM_DEPTH);
		pool.wait_for_tasks();

		auto end = std::chrono::high_resolution_clock::now();
		const auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

		std::cout << "population time: " << time << "ms\n";
	}
}

Board Com::doMove()
{
	auto start = std::chrono::high_resolution_clock::now();
	const auto node = m_tree->negamax(m_board, COM_DEPTH, EVAL_MIN, EVAL_MAX);

	auto end = std::chrono::high_resolution_clock::now();
	const auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

	std::cout << "negamax time: " << time << "ms\n";

	recordMove(node.first, COM_COLOR);

	return m_board;
}

