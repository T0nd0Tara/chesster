#pragma once
#include <pch.h>
#include <BS_thread_pool.hpp>

#include "./StateTree.h"
#include "../Board.h"

#define COM_DEPTH (3)
static_assert(COM_DEPTH > 1);

#define THREADS_COUNT (std::thread::hardware_concurrency() - 2)


class Com
{
public:
	Com(Board board = Board());
	Com(Com&) = default;
	Com& operator=(Com&&) = default;

	void init(Move);
	inline void setBoard(Board board) {
		m_board = board;
	}

	static eval_t evalBoard(Board board);

	void recordMove(Move, Color);

	Board doMove();

	static BS::thread_pool pool;
private:
	static const eval_t pieceValues[];

	std::unique_ptr<StateTree> m_tree;
	Board m_board;
};
