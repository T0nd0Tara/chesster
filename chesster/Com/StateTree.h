#pragma once
#include <pch.h>
#include <BS_thread_pool.hpp>

#include "../Move.h"

class Com;
class Board;

class StateTree
{
public:
	StateTree(const Move move = NULL_MOVE);

	// TODO: combine populate and negamax
	void populate(Board board, int depth);

	std::pair<Move, eval_t> negamax(Board board, size_t depth, eval_t alpha, eval_t beta) const;
	
	inline void clear();
	inline void clearChildren();

	std::string str() const;

	// TODO: change theire names as they are public
	std::vector<std::unique_ptr<StateTree>> m_children;

	Move m_move;

#if 0
public:
	StateTree& operator=(StateTree&& rhs) noexcept
	{
		m_move = std::move(rhs.m_move);

		std::vector<StateTree> temp = std::move(rhs.m_children);
		m_children = std::move(temp);

		return *this;
	}
#endif

private:
	static BS::thread_pool& pool;
};

