#include <pch.h>
#include "StateTree.h"
#include "Com.h"

BS::thread_pool& StateTree::pool = std::ref(Com::pool);

StateTree::StateTree(const Move move)
	: m_move(move)
{}

void StateTree::populate(Board board, int depth)
{

	if (m_children.empty()) {
		std::vector<Move> vMoves = board.getAvailMoves(board.getTurn());

		m_children.reserve(vMoves.size());
		for (const auto move : vMoves) {
			m_children.push_back(std::move(std::make_unique<StateTree>(move)));
		}
	}


	if (depth <= 0) return;

	//const Color opColor = color == Color::WHITE ? Color::BLACK : Color::WHITE;
	for (auto& child : m_children)
	{
		pool.push_task(
			[board, depth, &child]() {
				Board allegedBoard(board);
				allegedBoard.doMove(child->m_move, false, true);
				child->populate(allegedBoard, depth - 1);
			}
		);
	}
}

std::pair<Move, eval_t> StateTree::negamax(Board board, size_t depth, eval_t alpha, eval_t beta) const
{
	if (depth == 0 || m_children.size() == 0) {

		return std::make_pair(m_move, Com::evalBoard(board));
	}

	eval_t value = EVAL_MIN;
	Move move = NULL_MOVE;

	// TODO: delete me
	std::vector<std::pair<Move, eval_t>> vec;
	for (const auto& child : m_children) {
		std::pair<Move, eval_t> childValue;

		{
			Board allegedBoard(board);
			allegedBoard.doMove(child->m_move, false, true);
			childValue = child->negamax(allegedBoard, depth - 1, -beta, -alpha);

			childValue.second = - childValue.second;
		}

		vec.push_back(childValue);

		if (childValue.second > value) {
			value = childValue.second;
			move = child->m_move;
		}

		if (value > alpha)
			alpha = value;

		if (alpha >= beta)
			break;
	}

	return std::make_pair(move, value);
}

inline void StateTree::clear()
{
	clearChildren();

	m_move = NULL_MOVE;
}

inline void StateTree::clearChildren()
{
	m_children.clear();
}

std::string StateTree::str() const {
	std::string out = m_move.algebraicNotation() + "\n";

	for (const auto& child : m_children) {
		std::stringstream ss(child->str());
		std::string line;

		while (std::getline(ss, line, '\n'))
		{
			out += "\t" + line + "\n";
		}
	}

	return out;
}
