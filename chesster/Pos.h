#pragma once

#include "./pch.h"
#include "./common.h"
#include <magic_enum.hpp>

#define NULL_POS Pos()
#define NULL_POS_INDEX -1


struct Pos
{
	explicit Pos(int8_t i = NULL_POS_INDEX)
		: Pos(i % 8, i / 8)
	{}

	explicit Pos(SquareNotation i)
		: Pos((int8_t)i % 8, (int8_t)i / 8)
	{}

	Pos(const Pos&) = default;

	Pos(int8_t x, int8_t y)
	{
		xy.x = x;
		xy.y = y;
	}

	Pos(olc::vt2d pos)
		: Pos(pos.x, pos.y)
	{ }

	inline olc::vt2d toVec() const
	{
		return olc::vt2d{ x(), y() };
	}

	inline int8_t x() const
	{
		return xy.x;
	}

	inline int8_t y() const
	{
		return xy.y;
	}

	inline int8_t index() const
	{
		return y() * BOARD_SIZE + x();
	}

	inline SquareNotation square() const
	{
		return (SquareNotation)index();
	}

	// todo: deprecate this function
	inline std::string algebraicNotation() const
	{
		return std::string(magic_enum::enum_name(square()));
	}

public:
	// OPERATORS
	Pos& operator=(const Pos& pos) = default;

	inline bool operator==(const Pos& rhs) const
	{
		return xy == rhs.xy;
	}

	inline bool operator!=(const Pos& rhs) const
	{
		return !(xy == rhs.xy);
	}

	inline Pos operator+(const Pos& rhs) const
	{
		return Pos(toVec() + rhs.toVec());
	}

	inline Pos operator*(int8_t n) const
	{
		return Pos(n * toVec());
	}

	friend inline Pos operator*(int8_t n, const Pos& rhs)
	{
		return Pos(n * rhs.toVec());
	}

private:
	struct XY
	{
		int8_t x : 4;
		int8_t y : 4;

		inline bool operator==(const XY& xy) const {
			return x == xy.x && y == xy.y;
		}
	} xy;
};

inline bool inBoardBounds(Pos pos) {
	return inBoardBounds(pos.x(), pos.y());
}

