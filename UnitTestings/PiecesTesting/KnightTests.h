#pragma once
#include "../pch.h"
#include "../common.h"

TEST(Knight, getMoves_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 4, 4);
	
	std::vector<Move> expected{
		Move(Pos(e4), Pos(d2)),
		Move(Pos(e4), Pos(f2)),
		Move(Pos(e4), Pos(c3)),
		Move(Pos(e4), Pos(c5)),
		Move(Pos(e4), Pos(d6)),
		Move(Pos(e4), Pos(f6)),
		Move(Pos(e4), Pos(g5)),
		Move(Pos(e4), Pos(g3))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Knight, getMoves_secluded) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 4, 4);

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 3);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 4, 3);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 5, 3);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 4);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 5, 4);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 5);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 4, 5);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 5, 5);
	
	std::vector<Move> expected{
		Move(Pos(e4), Pos(d2)),
		Move(Pos(e4), Pos(f2)),
		Move(Pos(e4), Pos(c3)),
		Move(Pos(e4), Pos(c5)),
		Move(Pos(e4), Pos(d6)),
		Move(Pos(e4), Pos(f6)),
		Move(Pos(e4), Pos(g5)),
		Move(Pos(e4), Pos(g3))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Knight, getMoves_boardSide) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 7, 4);
	
	std::vector<Move> expected{
		Move(Pos(h4), Pos(g6)),
		Move(Pos(h4), Pos(f5)),
		Move(Pos(h4), Pos(f3)),
		Move(Pos(h4), Pos(g2))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(h4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Knight, getMoves_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), 4, 4);

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 3, 6);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 5, 6);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 2, 5);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 2, 3);

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 2);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 5, 2);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 6, 3);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 6, 5);
	
	std::vector<Move> expected{
		Move(Pos(e4), Pos(d6), Pos(d6)),
		Move(Pos(e4), Pos(f6), Pos(f6)),
		Move(Pos(e4), Pos(g5), Pos(g5)),
		Move(Pos(e4), Pos(g3), Pos(g3))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}
