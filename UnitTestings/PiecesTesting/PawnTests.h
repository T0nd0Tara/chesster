#pragma once
#include "../pch.h"
#include "../common.h"

TEST(Pawn, getMoves_sideEating_white) {
	Board b;
	b.init();

	b.doMove(Move(Pos(d2), Pos(d4)));
	b.doMove(Move(Pos(e7), Pos(e5)));

	// avail moves for (3,4)
	std::vector<Move> expected{
		Move(Pos(d4), Pos(e5), Pos(e5)),
		Move(Pos(d4), Pos(d5))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_sideEating_black) {
	Board b;
	b.init();

	b.doMove(Move(Pos(e2), Pos(e4)));
	b.doMove(Move(Pos(d7), Pos(d5)));
	b.doMove(Move(Pos(f2), Pos(f3)));

	// avail moves for (3,3)
	std::vector<Move> expected{
		Move(Pos(d5), Pos(d4)),
		Move(Pos(d5), Pos(e4), Pos(e4))  
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(d5));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_unPassant_white) {
	Board b;
	b.init();

	b.doMove(Move(Pos(d2), Pos(d4)));
	b.doMove(Move(Pos(b7), Pos(b6)));
	b.doMove(Move(Pos(d4), Pos(d5)));
	b.doMove(Move(Pos(e7), Pos(e5)));

	// avail moves for (3,3)
	std::vector<Move> expected{
		Move(Pos(d5), Pos(d6)),
		Move(Pos(d5), Pos(e6), Pos(e5))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(d5));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_unPassant_black) {
	Board b;
	b.init();

	b.doMove(Move(Pos(b2), Pos(b3)));
	b.doMove(Move(Pos(e7), Pos(e5)));
	b.doMove(Move(Pos(b3), Pos(b4)));
	b.doMove(Move(Pos(e5), Pos(e4)));
	b.doMove(Move(Pos(d2), Pos(d4)));

	// avail moves for (4, 4)
	std::vector<Move> expected{
		Move(Pos(e4), Pos(e3)),
		Move(Pos(e4), Pos(d3), Pos(d4))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_unPassant_afterFirstMove) {
	Board b;
	b.init();

	b.doMove(Move(Pos(e2), Pos(e4)));
	b.doMove(Move(Pos(d7), Pos(d5)));
	b.doMove(Move(Pos(e4), Pos(e5)));
	b.doMove(Move(Pos(f7), Pos(f5)));

	// avail moves for (4,3)
	std::vector<Move> expected{
		Move(Pos(e5), Pos(e6)),
		Move(Pos(e5), Pos(f6), Pos(f5))  
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e5));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_startMoves) {
	Board b;
	b.init();

	std::vector<Move> expected{
		Move(Pos(e2), Pos(e3)),
		Move(Pos(e2), Pos(e4))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e2));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_nonStartMoves) {
	Board b;
	b.init();

	b.doMove(Move(Pos(d2), Pos(d4)));
	b.doMove(Move(Pos(e7), Pos(e6)));

	std::vector<Move> expected {
		Move(Pos(d4), Pos(d5))  
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}

#pragma region promote
TEST(Pawn, getMoves_promote_white) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), olc::vt2d{ 7, 1 });

	std::vector<Move> expected{
		Move(Pos(h7), Pos(h8), NULL_POS, PieceType::QUEEN),
		Move(Pos(h7), Pos(h8), NULL_POS, PieceType::BISHOP),
		Move(Pos(h7), Pos(h8), NULL_POS, PieceType::KNIGHT),
		Move(Pos(h7), Pos(h8), NULL_POS, PieceType::ROOK)
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(h7));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_promote_black) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), olc::vt2d{ 1, 6 });

	std::vector<Move> expected{
		Move(Pos(b2), Pos(b1), NULL_POS, PieceType::QUEEN),
		Move(Pos(b2), Pos(b1), NULL_POS, PieceType::BISHOP),
		Move(Pos(b2), Pos(b1), NULL_POS, PieceType::KNIGHT),
		Move(Pos(b2), Pos(b1), NULL_POS, PieceType::ROOK)  
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(b2));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_promote_white_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), olc::vt2d{ 2, 1 });
	b.setPiece(Piece::create(PieceType::ROOK, Color::BLACK), olc::vt2d{ 3, 0 });

	std::vector<Move> expected{
		Move(Pos(c7), Pos(c8), NULL_POS, PieceType::QUEEN),
		Move(Pos(c7), Pos(c8), NULL_POS, PieceType::BISHOP),
		Move(Pos(c7), Pos(c8), NULL_POS, PieceType::KNIGHT),
		Move(Pos(c7), Pos(c8), NULL_POS, PieceType::ROOK),
		Move(Pos(c7), Pos(d8), Pos(d8), PieceType::QUEEN),
		Move(Pos(c7), Pos(d8), Pos(d8), PieceType::BISHOP),
		Move(Pos(c7), Pos(d8), Pos(d8), PieceType::KNIGHT),
		Move(Pos(c7), Pos(d8), Pos(d8), PieceType::ROOK)
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(c7));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Pawn, getMoves_promote_black_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), olc::vt2d{ 2, 6 });
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), olc::vt2d{ 3, 7 });

	std::vector<Move> expected{
		Move(Pos(c2), Pos(c1), NULL_POS, PieceType::QUEEN),
		Move(Pos(c2), Pos(c1), NULL_POS, PieceType::BISHOP),
		Move(Pos(c2), Pos(c1), NULL_POS, PieceType::KNIGHT),
		Move(Pos(c2), Pos(c1), NULL_POS, PieceType::ROOK),
		Move(Pos(c2), Pos(d1), Pos(d1), PieceType::QUEEN),
		Move(Pos(c2), Pos(d1), Pos(d1), PieceType::BISHOP),
		Move(Pos(c2), Pos(d1), Pos(d1), PieceType::KNIGHT),
		Move(Pos(c2), Pos(d1), Pos(d1), PieceType::ROOK)  
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(c2));

	ASSERT_VECTOR_EQ(actual, expected);
}

#pragma endregion promote
