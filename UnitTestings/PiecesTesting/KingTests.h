#pragma once
#include "../pch.h"
#include "../common.h"

TEST(King, getMoves_regular) {
	Board b;
    b.setPiece(Piece::create(PieceType::KING, Color::BLACK), 0, 0);
    b.setPiece(Piece::create(PieceType::KING, Color::WHITE), 4, 4);

	std::vector<Move> expected{
		Move(Pos(e4), Pos(d5)),
        Move(Pos(e4), Pos(e5)),
        Move(Pos(e4), Pos(f5)),
        Move(Pos(e4), Pos(d4)),
        Move(Pos(e4), Pos(f4)),
        Move(Pos(e4), Pos(d3)),
        Move(Pos(e4), Pos(e3)),
        Move(Pos(e4), Pos(f3)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(King, getMoves_corner) {
	Board b;
    b.setPiece(Piece::create(PieceType::KING, Color::BLACK), 0, 0);
    b.setPiece(Piece::create(PieceType::KING, Color::WHITE), 7, 0);

	std::vector<Move> expected{
        Move(Pos(h8), Pos(g8)),
        Move(Pos(h8), Pos(g7)),
        Move(Pos(h8), Pos(h7))
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(h8));

	ASSERT_VECTOR_EQ(actual, expected);
}

// TODO: test eating
