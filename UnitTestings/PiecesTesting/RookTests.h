#pragma once
#include "../pch.h"
#include "../common.h"

TEST(Rook, getMoves_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), 3, 4);
	
	std::vector<Move> expected{
        Move(Pos(d4), Pos(d5)),
        Move(Pos(d4), Pos(d6)),
        Move(Pos(d4), Pos(d7)),
        Move(Pos(d4), Pos(d8)),
        Move(Pos(d4), Pos(d3)),
        Move(Pos(d4), Pos(d2)),
        Move(Pos(d4), Pos(d1)),
        Move(Pos(d4), Pos(c4)),
        Move(Pos(d4), Pos(b4)),
        Move(Pos(d4), Pos(a4)),
        Move(Pos(d4), Pos(e4)),
        Move(Pos(d4), Pos(f4)),
        Move(Pos(d4), Pos(g4)),
        Move(Pos(d4), Pos(h4)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Rook, getMoves_corner) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), 7, 1);
	
	std::vector<Move> expected{
        Move(Pos(h7), Pos(h8)),
        Move(Pos(h7), Pos(h6)),
        Move(Pos(h7), Pos(h5)),
        Move(Pos(h7), Pos(h4)),
        Move(Pos(h7), Pos(h3)),
        Move(Pos(h7), Pos(h2)),
        Move(Pos(h7), Pos(h1)),
        Move(Pos(h7), Pos(g7)),
        Move(Pos(h7), Pos(f7)),
        Move(Pos(h7), Pos(e7)),
        Move(Pos(h7), Pos(d7)),
        Move(Pos(h7), Pos(c7)),
        Move(Pos(h7), Pos(b7)),
        Move(Pos(h7), Pos(a7)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(h7));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Rook, getMoves_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), 3, 4);

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 6, 4);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 5);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 2, 4);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 3, 1);
	
	std::vector<Move> expected{
        Move(Pos(d4), Pos(d5)),
        Move(Pos(d4), Pos(d6)),
        Move(Pos(d4), Pos(d3), Pos(d3)),
        Move(Pos(d4), Pos(e4)),
        Move(Pos(d4), Pos(f4)),
        Move(Pos(d4), Pos(g4), Pos(g4)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}
