#pragma once
#include "../pch.h"
#include "../common.h"

TEST(Queen, getMoves_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::QUEEN, Color::WHITE), 3, 4);
	
	std::vector<Move> expected{
        Move(Pos(d4), Pos(d5)),
        Move(Pos(d4), Pos(d6)),
        Move(Pos(d4), Pos(d7)),
        Move(Pos(d4), Pos(d8)),
        Move(Pos(d4), Pos(d3)),
        Move(Pos(d4), Pos(d2)),
        Move(Pos(d4), Pos(d1)),
        Move(Pos(d4), Pos(c4)),
        Move(Pos(d4), Pos(b4)),
        Move(Pos(d4), Pos(a4)),
        Move(Pos(d4), Pos(e4)),
        Move(Pos(d4), Pos(f4)),
        Move(Pos(d4), Pos(g4)),
        Move(Pos(d4), Pos(h4)),
        Move(Pos(d4), Pos(c5)),
        Move(Pos(d4), Pos(b6)),
        Move(Pos(d4), Pos(a7)),
        Move(Pos(d4), Pos(e5)),
        Move(Pos(d4), Pos(f6)),
        Move(Pos(d4), Pos(g7)),
        Move(Pos(d4), Pos(h8)),
        Move(Pos(d4), Pos(c3)),
        Move(Pos(d4), Pos(b2)),
        Move(Pos(d4), Pos(a1)),
        Move(Pos(d4), Pos(e3)),
        Move(Pos(d4), Pos(f2)),
        Move(Pos(d4), Pos(g1))  
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Queen, getMoves_corner) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::QUEEN, Color::WHITE), 7, 6);
	
	std::vector<Move> expected{
        Move(Pos(h2), Pos(h3)),
        Move(Pos(h2), Pos(h4)),
        Move(Pos(h2), Pos(h5)),
        Move(Pos(h2), Pos(h6)),
        Move(Pos(h2), Pos(h7)),
        Move(Pos(h2), Pos(h8)),
        Move(Pos(h2), Pos(h1)),
        Move(Pos(h2), Pos(g2)),
        Move(Pos(h2), Pos(f2)),
        Move(Pos(h2), Pos(e2)),
        Move(Pos(h2), Pos(d2)),
        Move(Pos(h2), Pos(c2)),
        Move(Pos(h2), Pos(b2)),
        Move(Pos(h2), Pos(a2)),
        Move(Pos(h2), Pos(g3)),
        Move(Pos(h2), Pos(f4)),
        Move(Pos(h2), Pos(e5)),
        Move(Pos(h2), Pos(d6)),
        Move(Pos(h2), Pos(c7)),
        Move(Pos(h2), Pos(b8)),
        Move(Pos(h2), Pos(g1))
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(h2));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Queen, getMoves_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::QUEEN, Color::WHITE), 3, 4);
	for (int i = 0; i < 4; i++) {
		b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), i + 1, 2);
		b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), i + 2, 6);
		b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 1, i + 3);
		b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 5, i + 2);
	}
	
	std::vector<Move> expected{
        Move(Pos(d4), Pos(d5)),
        Move(Pos(d4), Pos(d6), Pos(d6)),
        Move(Pos(d4), Pos(d3)),
        Move(Pos(d4), Pos(c4)),
        Move(Pos(d4), Pos(e4)),
        Move(Pos(d4), Pos(f4), Pos(f4)),
        Move(Pos(d4), Pos(c5)),
        Move(Pos(d4), Pos(b6), Pos(b6)),
        Move(Pos(d4), Pos(e5)),
        Move(Pos(d4), Pos(f6), Pos(f6)),
        Move(Pos(d4), Pos(c3)),
        Move(Pos(d4), Pos(e3)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(d4));

	ASSERT_VECTOR_EQ(actual, expected);
}
