#pragma once
#include "../pch.h"
#include "../common.h"

TEST(Bishop, getMoves_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), 4, 4);
	
	std::vector<Move> expected{
        Move(Pos(e4), Pos(d5)),
        Move(Pos(e4), Pos(c6)),
        Move(Pos(e4), Pos(b7)),
        Move(Pos(e4), Pos(a8)),
        Move(Pos(e4), Pos(f5)),
        Move(Pos(e4), Pos(g6)),
        Move(Pos(e4), Pos(h7)),
        Move(Pos(e4), Pos(d3)),
        Move(Pos(e4), Pos(c2)),
        Move(Pos(e4), Pos(b1)),
        Move(Pos(e4), Pos(f3)),
        Move(Pos(e4), Pos(g2)),
        Move(Pos(e4), Pos(h1))
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Bishop, getMoves_corner) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), 7, 0);
	
	std::vector<Move> expected{
        Move(Pos(h8), Pos(g7)),
        Move(Pos(h8), Pos(f6)),
        Move(Pos(h8), Pos(e5)),
        Move(Pos(h8), Pos(d4)),
        Move(Pos(h8), Pos(c3)),
        Move(Pos(h8), Pos(b2)),
        Move(Pos(h8), Pos(a1)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(h8));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Bishop, getMoves_eating) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), 4, 4);

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 6, 6);
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), 5, 3);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 1, 1);
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), 3, 5);

	std::vector<Move> expected{
        Move(Pos(e4), Pos(d5)),
        Move(Pos(e4), Pos(c6)),
        Move(Pos(e4), Pos(b7), Pos(b7)),
        Move(Pos(e4), Pos(d3), Pos(d3)),
        Move(Pos(e4), Pos(f3)) 
    };

	std::vector<Move> actual = b.getAvailMoves(Pos(e4));

	ASSERT_VECTOR_EQ(actual, expected);
}
