#pragma once

#include <Piece.h>
#include <Piece.cpp>

#include <Board.h>
#include <Board.cpp>

// assert that both vectors contain the excat same order
// not matter the order
#define ASSERT_VECTOR_EQ(a, b) \
	ASSERT_EQ(a.size(), b.size()); \
	ASSERT_TRUE(std::is_permutation(a.begin(), a.end(), b.begin()))
