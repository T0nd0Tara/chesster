#pragma once
#include "pch.h"
#include "common.h"

// TODO: test castle all sides
// TODO: test casle if goes through check, all sides
// TODO: test casle if ends in check, all sides
#pragma region check
TEST(Board, isChecked) {
	Board b;
	b.init();

    b.doMove(Move(Pos(e2), Pos(e3)));
	b.doMove(Move(Pos(d7), Pos(d6)));
	b.doMove(Move(Pos(f1), Pos(b5)));
	
	ASSERT_TRUE(b.isChecked(Color::BLACK));
} 

TEST(Board, isMoveCheck_alreadyInCheck) {
	Board b;
	b.init();

    b.doMove(Move(Pos(e2), Pos(e3)));
	b.doMove(Move(Pos(d7), Pos(d6)));
	b.doMove(Move(Pos(f1), Pos(b5)));
	
	ASSERT_TRUE(b.isMoveCheck(Move(Pos(b8), Pos(a6))));
} 

TEST(Board, isMoveCheck_createCheck) {
	Board b;
	b.init();

	b.doMove(Move(Pos(e2), Pos(e3)));
	b.doMove(Move(Pos(g7), Pos(g6)));
	b.doMove(Move(Pos(f1), Pos(b5)));

	ASSERT_TRUE(b.isMoveCheck(Move(Pos(d7), Pos(d6))));
}
#pragma endregion check
#pragma region castle
TEST(Board, castle_regular_left) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(a1));
	b.addCastleAbility(Board::CastleAbility::QUEEN, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)),
		Move(Pos(e1), Pos(f1)),
		Move(Pos(e1), Pos(c1))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_regular_left_apply) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(a1));
	b.addCastleAbility(Board::CastleAbility::QUEEN, Color::WHITE);

	const Piece whiteKing = b.getPiece(Pos(e1));
	const Piece rook = b.getPiece(Pos(a1));

	b.doMove(Move(Pos(e1), Pos(c1)), false);
	
	ASSERT_EQ(whiteKing, b.getPiece(Pos(c1)));
	ASSERT_EQ(rook, b.getPiece(Pos(d1)));
}

TEST(Board, castle_regular_right) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(h1));
	b.addCastleAbility(Board::CastleAbility::KING, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)),
		Move(Pos(e1), Pos(f1)),
		Move(Pos(e1), Pos(g1))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_regular_right_apply) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(h1));
	b.addCastleAbility(Board::CastleAbility::KING, Color::WHITE);

	const Piece whiteKing = b.getPiece(Pos(e1));
	const Piece rook = b.getPiece(Pos(h1));

	b.doMove(Move(Pos(e1), Pos(g1)), false);
	
	ASSERT_EQ(whiteKing, b.getPiece(Pos(g1)));
	ASSERT_EQ(rook, b.getPiece(Pos(f1)));
}

TEST(Board, castle_goesThroughCheck_left_adjacent) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(a1));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(b3));
	b.addCastleAbility(Board::CastleAbility::QUEEN, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(f1)) 
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_goesThroughCheck_left_endPoint) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(a1));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(a3));
	b.addCastleAbility(Board::CastleAbility::QUEEN, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)),
		Move(Pos(e1), Pos(f1))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_goesThroughCheck_right_adjacent) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(h1));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(h3));
	b.addCastleAbility(Board::CastleAbility::KING, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)) 
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_goesThroughCheck_right_endPoint) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(h1));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(h2));
	b.addCastleAbility(Board::CastleAbility::KING, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(e2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)),
		Move(Pos(e1), Pos(f1))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_white_while_in_check) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(a1));
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(h1));
	b.setPiece(Piece::create(PieceType::ROOK, Color::BLACK), Pos(e4));
	b.addCastleAbility(Board::CastleAbility::BOTH, Color::WHITE);

	std::vector<Move> expected{
		Move(Pos(e1), Pos(d2)),
		Move(Pos(e1), Pos(f2)),
		Move(Pos(e1), Pos(d1)),
		Move(Pos(e1), Pos(f1))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e1));

	ASSERT_VECTOR_EQ(actual, expected);
}

TEST(Board, castle_black_while_in_check) {
	Board b;
	b.initKings();
	b.setPiece(Piece::create(PieceType::ROOK, Color::BLACK), Pos(a8));
	b.setPiece(Piece::create(PieceType::ROOK, Color::BLACK), Pos(h8));
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(e4));
	b.addCastleAbility(Board::CastleAbility::BOTH, Color::BLACK);

	std::vector<Move> expected{
		Move(Pos(e8), Pos(d8)),
		Move(Pos(e8), Pos(f8)),
		Move(Pos(e8), Pos(d7)),
		Move(Pos(e8), Pos(f7))
	};

	std::vector<Move> actual = b.getAvailMoves(Pos(e8));

	ASSERT_VECTOR_EQ(actual, expected);
}
#pragma endregion castle

#pragma region pawns

#pragma region doubled
TEST(Board, doubled_pawns_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(b7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(d7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(d3));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(g2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(e2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(e6));

	const auto [whitePawns, blackPawns] = b.getDoubledPawns();

	ASSERT_EQ(whitePawns, 2);
	ASSERT_EQ(blackPawns, 2);
}

TEST(Board, doubled_pawns_blocked) {
	Board b;
	b.initKings();

	b.initKings();
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c7));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(c6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(c5));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(c4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(c2));

	const auto [whitePawns, blackPawns] = b.getDoubledPawns();

	ASSERT_EQ(whitePawns, 1);
	ASSERT_EQ(blackPawns, 1);
}

TEST(Board, doubled_pawns_quadrouple) {
	Board b;

	b.initKings();
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c3));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f3));

	const auto [whitePawns, blackPawns] = b.getDoubledPawns();

	ASSERT_EQ(whitePawns, 3);
	ASSERT_EQ(blackPawns, 3);
}
#pragma endregion doubled

#pragma region isolated
TEST(Board, isolated_pawns_regular) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(b7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c3));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(d6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(e7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(g7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(h6));
	b.setPiece(Piece::create(PieceType::ROOK, Color::WHITE), Pos(f7));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(g2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f6));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(e3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(d2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(b2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(a3));
	b.setPiece(Piece::create(PieceType::ROOK, Color::BLACK), Pos(c2));

	const auto [whitePawns, blackPawns] = b.getIsolatedPawns();

	ASSERT_EQ(whitePawns, 3);
	ASSERT_EQ(blackPawns, 3);
}

TEST(Board, isolated_pawns_side_touch) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(b7));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(g2));

	const auto [whitePawns, blackPawns] = b.getIsolatedPawns();

	ASSERT_EQ(whitePawns, 0);
	ASSERT_EQ(blackPawns, 0);
}

TEST(Board, isolated_pawns_diagonal_touch) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(f6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(g7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(h6));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(c3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(b2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(a3));

	const auto [whitePawns, blackPawns] = b.getIsolatedPawns();

	ASSERT_EQ(whitePawns, 0);
	ASSERT_EQ(blackPawns, 0);
}

TEST(Board, isolated_pawns_nearby_pieces) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(d6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(e7));

	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(e3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(d2));

	const auto [whitePawns, blackPawns] = b.getIsolatedPawns();

	ASSERT_EQ(whitePawns, 1);
	ASSERT_EQ(blackPawns, 1);
}

TEST(Board, isolated_pawns_isolated) {
	Board b;
	b.initKings();

	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c3));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f6));

	const auto [whitePawns, blackPawns] = b.getIsolatedPawns();

	ASSERT_EQ(whitePawns, 2);
	ASSERT_EQ(blackPawns, 2);
}
#pragma endregion isolated

#pragma region blocked
TEST(Board, blocked_pawns_blocked)
{
	Board b;
	b.initKings();

	// TODO: split this to multiple tests for easier debugging
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c7));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(c6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(e3));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(e2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(g5));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), Pos(g4));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h3));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f3));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(f4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(d6));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(d7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(b4));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(b5));

	const auto [whitePawns, blackPawns] = b.getBlockedPawns();

	ASSERT_EQ(whitePawns, 5);
	ASSERT_EQ(blackPawns, 5);
}

TEST(Board, blocked_pawns_not_blocked)
{
	Board b;
	b.initKings();

	// TODO: split this to multiple tests for easier debugging
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a7));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(a5));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(c7));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(c4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(e3));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::BLACK), Pos(e1));
	b.setPiece(Piece::create(PieceType::PAWN, Color::BLACK), Pos(g6));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::WHITE), Pos(g1));

	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h2));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(h4));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(f3));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(f6));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(d6));
	b.setPiece(Piece::create(PieceType::KNIGHT, Color::WHITE), Pos(d8));
	b.setPiece(Piece::create(PieceType::PAWN, Color::WHITE), Pos(b3));
	b.setPiece(Piece::create(PieceType::BISHOP, Color::BLACK), Pos(b8));

	const auto [whitePawns, blackPawns] = b.getBlockedPawns();

	ASSERT_EQ(whitePawns, 0);
	ASSERT_EQ(blackPawns, 0);
}
#pragma endregion blocked

#pragma endregion pawns

