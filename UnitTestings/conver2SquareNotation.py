from os import walk, path
import re

def convert_text(text):
    def repl(m):
        x = m.group(2)
        y = m.group(3)

        assert len(x) == 1 and len(y) == 1

        x = chr(ord(x) + ord('a') - ord('0'))
        y = ord('8') - ord(y)
        
        return f'Pos({x}{y})'
    
    regex = re.compile('Pos(\(|{) ?(\d), ?(\d) ?(\)|})')
    return regex.sub(repl, text)

        
        

def convert_file(dirpath, filename):
    text: str
    with open(path.join(dirpath, filename)) as fh:
        text = fh.read()

    text = convert_text(text)

    with open(path.join(dirpath, filename), 'w') as fh:
        fh.write(text)
        

for (dirpath, dirnames, filenames) in walk('.'):
    if 'Debug' in dirpath or 'x64' in dirpath:
        continue
    for filename in filenames:
        if not (filename.endswith('.cpp') or filename.endswith('.h')):
            continue
        print(f'converting file {filename}, in path {dirpath}')
        convert_file(dirpath, filename)

        
        
    
