#include "pch.h"

#define OLC_PGE_APPLICATION

#include "BoardTests.h"

#include "PiecesTesting/PawnTests.h"
#include "PiecesTesting/KnightTests.h"
#include "PiecesTesting/QueenTests.h"
#include "PiecesTesting/RookTests.h"
#include "PiecesTesting/BishopTests.h"
#include "PiecesTesting/KingTests.h"

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}